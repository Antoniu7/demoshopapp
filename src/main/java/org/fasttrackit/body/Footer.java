package org.fasttrackit.body;

public class Footer {
    private final String details;
    private final String questionIcon;
    private final String resetIconTitle;

    public Footer() {
        this.details = "Demo Shop | build date 2021-05-21 14:04:30 GTBDT";
        this.questionIcon = "?";
        this.resetIconTitle = "Reset the application state";
    }

    public String getDetails() {
        return this.details;
    }

    public String getQuestionIcon() {
        return questionIcon;
    }

    public String getResetIconTitle() {
        return resetIconTitle;
    }
}
