package org.fasttrackit.body;

public class Modal {
    private final String modalTitle;
    private final String closeButton;
    private final String user;
    private final String password;
    private final String loginButton;

    public Modal() {
        this.modalTitle = "Login";
        this.closeButton = "x";
        this.user = "user";
        this.password = "password";
        this.loginButton = "Login";
    }

    public String getModalTitle() {
        return modalTitle;
    }

    public void clickOnCloseButton() {
        System.out.println("----------");
        System.out.println("Clicked on the 'x' button");
    }

    public void validateModalComponents() {
        System.out.println("1. Verify that the Modal is displayed.");
        System.out.println("2. Verify that the Modal Title is: " + modalTitle);
        System.out.println("3. Verify that the Modal close button is: " + closeButton);
        System.out.println("4. Verify that the username field is displayed.");
        System.out.println("5. Verify that the password field is displayed.");
        System.out.println("6. Verify that the Login button is displayed.");
        System.out.println("7. Verify that the Login button is enabled.");
    }
}
