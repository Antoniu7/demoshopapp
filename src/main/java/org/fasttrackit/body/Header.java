package org.fasttrackit.body;

public class Header {
    private final String logoIconUrl;
    private final String shoppingCartIconUrl;

    private final String wishlistIconUrl;

    private final String greetingsMessage;

    private final String loginButton;

    public Header() {
        this.logoIconUrl = "/";
        this.shoppingCartIconUrl = "/cart";
        this.wishlistIconUrl = "/wishlist";
        this.greetingsMessage = "Hello guest!";
        this.loginButton = "sign-in";
    }

    /**
     * Getters
     */

    public String getLogoIconUrl() {
        return this.logoIconUrl;
    }

    public String getShoppingCartIconUrl() {
        return shoppingCartIconUrl;
    }

    public String getWishlistIconUrl() {
        return wishlistIconUrl;
    }

    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    public String getLoginButton() {
        return loginButton;
    }

    /**
     * Actions
     */
    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the login button.");
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Clicked on the wishlist button.");
    }
}
