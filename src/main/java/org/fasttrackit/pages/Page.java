package org.fasttrackit.pages;

import org.fasttrackit.DemoShopApp;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

public class Page {

    private final String title = DemoShopApp.DEMO_SHOP_TITLE;
    private final Header header;
    private final Footer footer;

    public Page() {
        this.header = new Header();
        this.footer = new Footer();
    }

    public Header getHeader() {
        return header;
    }

    public void verifyThatTitleIsDisplayedOnScreen() {
        System.out.println("----------");
        System.out.println("1. Verify that title is: " + title);
        System.out.println("----------");
    }

    public void validateThatFooterContainsAllElements() {
        System.out.println("----------");
        System.out.println("1. Verify footer details are: " + footer.getDetails());
        System.out.println("2. Verify footer has question icon: " + footer.getQuestionIcon());
        System.out.println("3. Verify footer reset icon title: " + footer.getResetIconTitle());
        System.out.println("----------");
    }

    public void validateThatHeaderContainsAllElements() {
        System.out.println("----------");
        System.out.println("1. Verify that logo url is: " + header.getLogoIconUrl());
        System.out.println("2. Verify that shopping cart url is: " + header.getShoppingCartIconUrl());
        System.out.println("3. Verify that wishlist url is: " + header.getWishlistIconUrl());
        System.out.println("4. Verify that welcome message is: " + header.getGreetingsMessage());
        System.out.println("5. Verify that sign-in is: " + header.getLoginButton());
        System.out.println("----------");
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
    }

    public void validateModalIsNotDisplayed() {
        System.out.println("1. Verify that modal is not on page.");
    }

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistIcon();
    }
}
