package org.fasttrackit;

import org.fasttrackit.body.Modal;
import org.fasttrackit.pages.Page;

public class DemoShopApp {
    public static final String APP_TITLE = " - = Demo Shop Testing Simulator = - ";

    public static final String DEMO_SHOP_TITLE = "Demo Shop";

    public static void main(String[] args) {
        System.out.println(APP_TITLE);
        Page homePage = new Page();
        homePage.verifyThatTitleIsDisplayedOnScreen();
        homePage.validateThatFooterContainsAllElements();
        homePage.validateThatHeaderContainsAllElements();

        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.validateModalComponents();
        modal.clickOnCloseButton();
        homePage.validateModalIsNotDisplayed();
        homePage.clickOnTheWishlistButton();
    }
}